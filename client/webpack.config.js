const path = require('path');


module.exports = {
    entry: './app/index.js',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'app.js'
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    },
    module: {
        loaders: [
            {
                test: /\.css$/, 
                loader: "style-loader!css-loader"
            }
        ]
    }
};
