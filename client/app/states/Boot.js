// import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js';

class Boot extends Phaser.State {
  create() {
    this.game.stage.backgroundColor = "#fff";
    this.game.state.start('preload');
  }
}

export default Boot;