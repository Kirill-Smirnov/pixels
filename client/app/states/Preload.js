class Preload extends Phaser.State {
  preload(game) {
    const logo =  this.load.image('logo', '../assets/logo.png');

    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);

    game.time.advancedTiming = true;
  }

  onLoadComplete() {
    this.game.state.start('menu', true, false);
  }
}

export default Preload;