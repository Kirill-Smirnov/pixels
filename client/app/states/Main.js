import HUD from '../gui/HUD.js';

import "phaser-kinetic-scrolling-plugin";

import * as pointManager from '../sockets.js';
import store from '../store';
import initPlugins from '../plugins';


class Main extends Phaser.State {
  create(game) {
    initPlugins(game);

    game.kineticScrolling.start();

    pointManager.askPoints({
      x: game.camera.x, 
      y: game.camera.y, 
      width: game.camera.x+game.camera.width,
      height: game.camera.y+game.camera.height
    });

    
    this.timer = 0;

    HUD.create(game);

    game.input.onTap.add(_ => {
      //TODO: Send to server.
      this.onClick(game);
    }, this);
    
    game.world.setBounds(0, 0, 2000, 2000);
    
    // CONTROLS
    this.game.kineticScrolling.start();
    // game.input.activePointer.move = e => {
    //   this.move(game, e);
    // };
  }

  update(game) {
    if (game.time.now > this.timer) {
      let graphics = game.add.graphics(0, 0);
      // graphics.lineStyle(1, "#000", 1);
      for (let point of store.points) {
        graphics.beginFill(point.color);
        graphics.drawRect(point.x*3, point.y*3, 3, 3);
      }

      this.timer = game.time.now + 1000;
    }

    // if (game.input.activePointer.isDown && game.origDragPoint) {
    //   game.camera.x += game.origDragPoint.x - game.input.activePointer.position.x;
    //   game.camera.y += game.origDragPoint.y - game.input.activePointer.position.y;
    // }
  }

  move(game, e) {
    if (true) {
      game.camera.x = e.offsetX;
      game.camera.y = e.offsetY;
    }
  }

  onClick(game) {
    console.log(`Coordinates are ${game.input.worldX} and ${game.input.game.input.worldY}`);
  }
}

export default Main;