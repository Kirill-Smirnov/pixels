import GUI from '../gui/GUI.js';

class Menu extends Phaser.State {
  init(game) {
  }

  create(game) {
    GUI.showForms();
  }

  static startGame(game) {
    GUI.hideForms();
    game.state.start('main', true, false);
  }
}

export default Menu;