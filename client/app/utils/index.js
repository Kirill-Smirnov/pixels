export default function (data) { 
  for (let datum of data) {
    if(datum.value === "") 
      return true;
  }
  return false;
}
