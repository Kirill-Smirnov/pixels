import scrolling from './scrolling.js';

export default function initPlugins(game) {
  scrolling.init(game);
}