const init = game => {
  game.kineticScrolling = game.plugins.add(Phaser.Plugin.KineticScrolling);

  game.kineticScrolling.configure({
    kineticMovement: true,
    timeConstantScroll: 325, //really mimic iOS
    horizontalScroll: true,
    verticalScroll: true,
    horizontalWheel: true,
    verticalWheel: false,
    deltaWheel: 40,
    onUpdate: null
  });
}

export default { init };