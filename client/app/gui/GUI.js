import $ from 'webpack-zepto';

import store from '../store';

export default class GUI {
  static showForms() {
    $("#gui-wrapper").css('visiblity', 'visible').show();
  }

  static hideForms() {
    $("#gui-wrapper").css('visiblity', 'hidden').hide();
  }
  static setError(msg) {
    store.errorField.html(msg);
  }
}