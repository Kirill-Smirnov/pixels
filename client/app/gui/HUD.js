// import { $, createEl } from '../utils';

export default class HUD {
  static create(game) {
    let logo = game.add.sprite(3, 3, 'logo');
    let ratio = logo.width/logo.height;
    logo.height = 30;
    logo.width = logo.height*ratio;
    logo.z = 999;
    logo.fixedToCamera = true;
  }
}