class Store {
  constructor() {
    this.points = [];
    this.users = [];
  }

  updatePoint(data) {
    this.points.push(data);
  }

  set points(points) {
    this._points = points;
  }

  get points() {
    return this._points;
  }

  set users(users) {
    this._users = users;
  }

  get users() {
    return this._users;
  }

  set user(user) {
    this._user = user;
  }

  get user() {
    return this._user;
  }

  set errorField(el) {
    this._errorField = el;
  }

  get errorField() {
    return this._errorField;
  }
}


var store = new Store();
export default store;