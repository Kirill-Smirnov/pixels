import $ from 'webpack-zepto';

import store from './store';
import game from './game.js';
import Menu from './states/Menu.js';
import isEmpty from './utils';
import GUI from './gui/GUI.js';

const auth = (selector, path) => {
  $(selector).submit(e => {
    e.preventDefault();
    let data = $(selector).serializeArray();
    if(isEmpty(data))
      GUI.setError("Empty input. Try again.");
    else {
      $.post(path, data, res => {
        if(res.error)
          GUI.setError(res.error);
        else {
          store.user = res.user;
          Menu.startGame(game);
        }
      });
    }
  });
}

(function authController() {
  store.errorField = $('#error-field');
  [['#signup', '/api/signup'],
  ['#login', '/api/login']].map((data, i, arr) => {
    auth(...data);
  });
})();