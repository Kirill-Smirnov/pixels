import io from 'socket.io-client';
import store from './store';

let socket = io("http://localhost:3000");

export function askPoints(coords) {
  socket.emit('getAll', coords);
}

export function createPoint(x, y, color, username) {
  socket.emit('create', { x, y, color, username });
}

socket.on('points', data => {
  // Redraw all
  store.points = data;
});

socket.on('point', data => {
  // Update one
  store.updatePoint(data);
});