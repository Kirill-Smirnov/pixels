import PIXI from 'expose-loader?PIXI!phaser-ce/build/custom/pixi.js';
import p2 from 'expose-loader?p2!phaser-ce/build/custom/p2.js';
import Phaser from 'expose-loader?Phaser!phaser-ce/build/custom/phaser-split.js';

import Preload from './states/Preload.js';
import Boot from './states/Boot.js';
import Menu from './states/Menu.js';
import Main from './states/Main.js';


var game = new Phaser.Game(
  window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
  window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
  Phaser.AUTO,
  'game'
);

game.state.add('boot', Boot);
game.state.add('preload', Preload);
game.state.add('menu', Menu);
game.state.add('main', Main);

game.state.start('boot');

export default game;