# Unlimited pixels
## Realization
### Backend
* Node.JS on backend
* Express as a framework
* MongoDB as a database
* Mongoose as an ORM
* bcrypt as password hash
### Frontend
* JS and ES6
* HTML5 Canvas for field
* Phaser.js for graphics, Zepto.js for GUI
### Misc
* nodemon for dev server


## _TODOS_
1. Add relationships btw user and his points _Does Mongo support it??_ _**Done**_
1. Think of a solution for problem with Mongo
1. Think about privateness of API. **IMPORTANT**
1. Login user after signuping in backend.