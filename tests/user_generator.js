const randomInt = (max, min) => Math.floor(Math.random() * (max - min)) + min;


function randomWord(length) {
  var word = '';
  for (let i =0; i < length; i++) {
    word += String.fromCharCode(randomInt(65, 122));
  }
  return word;
}

const request = require('request');

for (let i = 0; i < 20; i++) {
  request.post(
      'http://localhost:3000/api/signup/',
      { json: { username: randomWord(3), password: randomWord(8) } },
      function (error, response, body) {
          if (!error && response.statusCode == 200) {
              console.log(body)
          }
      }
  );

}
