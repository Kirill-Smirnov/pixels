const express = require('express');
const bodyParser = require('body-parser');


require("console-stamp")(console, { pattern: "HH:MM:ss" });

const db = require('./api/database.js');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// Configure bodyParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Socket.io
const sockets = require('./api/sockets.js')(io);

// Microservices
app.use(express.static('../client/'));
const Router = require('./routes.js');
app.use('/', Router);

const APIRouter = require('./api/routes.js');
app.use('/api', APIRouter);

const port = process.env.PORT || 3000;

http.listen(port, _ => console.log(`Server runned on port ${port}`));
