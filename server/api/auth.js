const bCrypt = require('bcrypt');

const User = require('../models/user.js');

const createHash = p => bCrypt.hashSync(p, bCrypt.genSaltSync(10), null);
const isPasswordValid = (u, p) => bCrypt.compareSync(p, u.password);

const login = (req, res) => {
  var username = req.body.username,
      password = req.body.password;

    User.findOne({ username: username }, (err, user) => {
      if (err)
        res.json({"error": "Unknown error."});
      else if (!user)
        res.json({"error": "User with this username does not exist."});
      else if (!isPasswordValid(user, password))
        res.json({"error": "Password does not match."});
      else {
        console.log("User " + username + " logged in.");
        res.json({"username": username, "message": "User authenticated."});
      }
    });
}

const signup = (req, res) => {
  var username = req.body.username,
      password = req.body.password,
      role = req.body.role;

    User.findOne({ username: username }, (err, user) => {
      if (err)
        res.json({"error": "Unknown error."});
      else if (user)
        res.json({"error": "User with this username already exists."});
      else {
        var user = new User();
        user.username = username;
        user.password = createHash(password);
        if (role) user.role = role;
        user.save();
        console.log("User " + username + " created.");
        res.json({"username": username, "message": "User created."});
      }
    });
}

module.exports = { login, signup }