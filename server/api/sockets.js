const User = require('../models/user.js');
const Point = require('../models/point.js');

module.exports = function(io) {
  io.on('connection', socket => {
    console.log('a user connected');

    socket.on('disconnect', _ => {
      console.log('user disconnected');
    });

    socket.on('create', data => {
      // TODO: Check if user can add
      Point.update({ x: data.x, y: data.y }, {$set: data }, { upsert: true },
      (err, point) => {
        io.emit('point', data);
      });
    });
    
    socket.on('getAll', data => {
      Point.find({ // Get everything in user's screen
          x: { $gt: data.x-1, $lt: data.width+1 },
          y: { $gt: data.y-1, $lt: data.height+1 }
      }, (err, points) => {
        io.emit('points', points);
      });
    });
  });
}
