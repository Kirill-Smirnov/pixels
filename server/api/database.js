const mongoose = require('mongoose');

mongoose.connect(process.env.dbpath || 'mongodb://localhost:27017', { useMongoClient: true });

const db = mongoose.connection;

db.on('error', err => {
  console.log('connection error:', err.message);
});
db.once('open', () => {
  //TODO: send error
  console.log("Connected to DB!");
});

module.exports = db;
