const express = require('express');
const router = express.Router();

 router.get('/', (req, res) => {
   res.json({ 'message': 'Welcome' });
 });

var Point = require('../models/point.js');
var User = require('../models/user.js');

// points
router.get('/points/', (req, res) => {
  Point.find((err, points) => {
    res.json(points);
  })
});

// router.post('/points/new/', (req, res) => {
//   var point = new Point({
//     x: req.body.x,
//     y: req.body.y,
//     color: req.body.color,
//     username: req.body.username,
//     time: new Date()
//   }).save();
//
//   res.json({ "message": "Point has been created"});
// });

// users

router.get('/users/', (req, res) => {
  User.find((err, users) => {
    res.json(users);
  })
});

// const createHash = require('./auth.js').createHash;
// const isPasswordValid = require('./auth.js').isPasswordValid;
const login = require('./auth.js').login;
const signup = require('./auth.js').signup;

router.post('/login/', login);

router.post('/signup/', signup);

 module.exports = router;
