const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = require('./user.js');

var Point = new Schema({
  x: Number,
  y: Number,
  color: String,
  username: String, //TODO: Fix relationships
  time: {
    type: Date,
    default: new Date
  }
})

module.exports = mongoose.model('point', Point);
