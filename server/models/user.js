const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var User = new Schema({
  username: String,
  password: String,
  role: {
    type: String,
    default: 'user'
  },
  lastTime: Number
})

module.exports = mongoose.model('user', User);
